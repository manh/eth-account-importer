import { useRef, useState } from 'react'
import { ethers } from 'ethers'

export default function Home() {
    const inputRef = useRef()
    const [account, setAccount] = useState({
        address: '',
        privateKey: '',
    })

    const onSubmit = e => {
        e.preventDefault()

        const { value: phrase } = inputRef.current
        if (phrase.split(' ').length !== 12)
            return alert('Invalid phrase. Please enter 12-character phrase')

        const wallet = ethers.Wallet.fromMnemonic(phrase)
        setAccount(wallet)
    }

    console.log(account)

    return (
        <div className="min-h-screen bg-slate-100">
            <div className="container mx-auto px-2 md:px-4 py-8">
                <form onSubmit={onSubmit}>
                    <label htmlFor="key">
                        Import your 12-character mnemoic phrase
                    </label>
                    <div className="w-full flex items-center">
                        <input
                            ref={inputRef}
                            id="key"
                            className="w-4/5 border bg-slate-200 px-4 py-3 rounded-l outline-none"
                            autoComplete="off"
                            placeholder="Import your 12-character mnemoic phrase"
                        />
                        <button
                            key="submit"
                            className="w-1/5 border border-blue-600 bg-blue-600 text-white px-4 py-3 rounded-r">
                            Import account
                        </button>
                    </div>
                </form>
                <div className="mt-8 bg-slate-200 px-4 py-3 text-sm text-slate-900">
                    Your account: {account.address}
                    <br />
                    Private key: {account.privateKey}
                    <div className="flex w-full items-center justify-center">
                        <button
                            onClick={() => {
                                navigator.clipboard.writeText(
                                    account.privateKey
                                )
                            }}
                            className="px-4 py-2 mt-4 text-sm bg-blue-600 text-white font-bold uppercase rounded">
                            Copy private key
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}
